﻿using System;
using System.Collections.Generic;

class Character
{
    protected string name = "Generic Guy";
    protected int hitpoints = 5;
    protected int damage = 1;

    public virtual bool IsAlive()
    {
        return (hitpoints > 0);
    }

    public int GetHitpoints()
    {
        return hitpoints;
    }

    public virtual void Hit(Character other)
    {
        other.hitpoints -= damage;
    }

    public virtual string GetClassName()
    {
        return "Hero";
    }

    public string GetName()
    {
        return name;
    }

    public virtual string GetBattleCry()
    {
        return "";
    }
}
