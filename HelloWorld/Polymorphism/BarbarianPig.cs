﻿using System;
using System.Collections.Generic;

class BarbarianPig : Character
{
    private string[] possibleNames = { "Babe", "Bacon", "Sir Oinksalot", "Porkchop"};

    public BarbarianPig(Random rnd)
    {
        hitpoints = 10;
        damage = 3;

        name = possibleNames[rnd.Next(0, possibleNames.Length)];
    }

    public override string GetClassName()
    {
        return "Barbarian Pig";
    }

    public override string GetBattleCry()
    {
        return "Oink, I'm bringing home the bacon or my name isn't " + name + "!!!";
    }
}
