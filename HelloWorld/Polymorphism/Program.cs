﻿using System;
using System.Collections.Generic;



class Program
{
    static void DoFight(Character firstGuy, Character secondGuy)
    {
        Console.WriteLine("A valiant " + firstGuy.GetClassName() + " enters the battlefield...");
        Console.WriteLine(firstGuy.GetBattleCry());

        Console.WriteLine("A strong " + secondGuy.GetClassName() + " enters the battlefield...");
        Console.WriteLine(secondGuy.GetBattleCry());

        // Fight!
        while (firstGuy.IsAlive() && secondGuy.IsAlive())
        {
            firstGuy.Hit(secondGuy);
            secondGuy.Hit(firstGuy);

            Console.WriteLine(firstGuy.GetName() + " hit so hard! " + secondGuy.GetName() + " is at " + secondGuy.GetHitpoints() + " hp left!");
            Console.WriteLine("{0} hit back! {1} is at {2} hp left!", secondGuy.GetName(), firstGuy.GetName(), firstGuy.GetHitpoints());
        }

        if (!firstGuy.IsAlive())
        {
            Console.WriteLine(firstGuy.GetName() + " is dead!");
        }
        if (!secondGuy.IsAlive())
        {
            Console.WriteLine(secondGuy.GetName() + " is dead!");
        }
    }

    static void Main(string[] args)
    {
        Random rnd = new Random();

        Character firstGuy = new BarbarianPig(rnd);

        Character secondGuy = new Character();

        // TODO: Set up the guys

        DoFight(firstGuy, secondGuy);

        Console.ReadLine();

    }
}