﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        int[] myArray = new int[30];

        List<int> myList = new List<int>();


        myList.Add(3);
        myList.Add(-8);
        myList.Add(6);
        myList.Add(6);
        myList.Add(1);
        myList.Add(-8);
        myList.Add(-8);

        for(int i = 0; i < myList.Count; ++i)
        {
            Console.WriteLine("Element #" + i + " is " + myList[i]);
        }

        myList.Remove(-8);

        Console.WriteLine("After removal: ");

        for (int i = 0; i < myList.Count; ++i)
        {
            Console.WriteLine("Element #" + i + " is " + myList[i]);
        }

        Dictionary<string, string> actualDictionary = new Dictionary<string, string>();

        actualDictionary.Add("banana", "A yellow fruit.");
        actualDictionary.Add("Lou", "Better than Jon at programming.");
        actualDictionary.Add("Zach", "Better than Jon at nothing.");

        //actualDictionary.ContainsKey("broccoli")

        string value;
        if(actualDictionary.TryGetValue("Lou", out value))
        {
            Console.WriteLine(value);
        }

        actualDictionary["Lou"] = "Not that much better than Jon.";
        Console.WriteLine(actualDictionary["Lou"]);

        Console.ReadLine();
    }
}