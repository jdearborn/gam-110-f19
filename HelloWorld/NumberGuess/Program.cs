﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Random rnd = new Random();

        int answer = rnd.Next(1, 101);

        int num = -1;

        while (num != answer)
        {
            Console.WriteLine("Hey, guess a number between 1 and 100:");
            string input = Console.ReadLine();

            if (int.TryParse(input, out num))
            {
                if (num < answer)
                    Console.WriteLine("That's too low!");
                else if (num > answer)
                    Console.WriteLine("That's too high!");
                else
                    Console.WriteLine("You got it!  You are a winner.");
            }
            else
            {
                Console.WriteLine("You call that a number?!");
            }
        }

        Console.ReadLine();
    }
}