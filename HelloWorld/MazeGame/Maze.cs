﻿using System;
using System.IO;


class Maze
{
    public char[,] tiles = { {'#', '!', '#', '#', '#', '#', '#', '#', '#', '#'},
                          {'!', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'},
                          {'#', ' ', ' ', ' ', ' ', ' ', '$', 'H', ' ', '#'},
                          {'#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'},
                          {'#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#'},
                          {'#', ' ', 'A', 'B', '#', '#', '#', ' ', ' ', '#'},
                          {'#', ' ', 'C', ' ', '#', ' ', '#', ' ', ' ', '#'},
                          {'#', ' ', '#', '#', '#', ' ', '#', ' ', ' ', '#'},
                          {'#', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', '#'},
                          {'#', '#', '#', '#', '#', '#', '#', '#', '#', '#'}};
    
    private int width;
    private int height;

    public string message;

    public Maze()
    {
        width = tiles.GetLength(1);
        height = tiles.GetLength(0);

        //Load("Level1.txt");

        for (int row = 0; row < height; ++row)
        {
            for (int col = 0; col < width; ++col)
            {
                // Replace every H with a Heart
                if(tiles[row, col] == 'H')
                {
                    tiles[row, col] = '\u2665';
                }
            }
        }
    }

    public bool Load(string filename)
    {
        StreamReader reader = new StreamReader(filename);

        tiles = new char[height, width];

        // Load level characters into my new maze
        int row = 0;
        int col = 0;
        while(reader.Peek() > -1)
        {
            char c = (char)reader.Read();
            if (c == '\r')
                continue;
            if(c == '\n')
            {
                col = 0;
                row++;
                continue;
            }

            tiles[row, col] = c;
        }

        Console.ReadLine();

        reader.Close();

        return true;
    }

    public void Draw()
    {
        for (int row = 0; row < height; ++row)
        {
            for (int col = 0; col < width; ++col)
            {
                Console.Write(tiles[row, col] + " ");
            }

            Console.WriteLine();
        }
        Console.WriteLine();
        Console.WriteLine(message);
    }

    public int GetWidth()
    {
        return width;
    }

    public int GetHeight()
    {
        return height;
    }

    public void SetTile(int row, int column, char value)
    {
        tiles[row, column] = value;
    }

    public char GetTile(int row, int col)
    {
        if (row < 0 || row >= GetHeight() || col < 0 || col >= GetWidth())
            return '#';

        return tiles[row, col];
    }

    public bool IsWalkable(int newRow, int newCol)
    {
        // Off the ends of the array?  Bad!
        if (newRow < 0 || newRow >= GetHeight() || newCol < 0 || newCol >= GetWidth())
            return false;

        if (tiles[newRow, newCol] == '#')
            return false;

        if (tiles[newRow, newCol] == 'D')
            return false;

        return true;
    }
}