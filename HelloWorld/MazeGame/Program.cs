﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.UTF8;

        Maze maze = new Maze();

        Player player = new Player();

        player.PutInMaze(maze);

        while(true)
        {

            Console.Clear();
            maze.Draw();


            Console.WriteLine("Press a direction");

            ConsoleKeyInfo key = Console.ReadKey(true);
            if(key.Key == ConsoleKey.Escape)
            {
                Console.WriteLine("See ya!");
                break;
            }
            if (key.Key == ConsoleKey.UpArrow)
            {
                player.MoveUp(maze);
            }
            if (key.Key == ConsoleKey.DownArrow)
            {
                player.MoveDown(maze);
            }
            if (key.Key == ConsoleKey.LeftArrow)
            {
                /*playerCol--;

                if (playerCol < 0)
                    playerCol = 0;*/
                player.MoveLeft(maze);
            }
            if (key.Key == ConsoleKey.RightArrow)
            {
                /*playerCol++;

                if (playerCol >= maze.GetWidth())
                    playerCol = maze.GetWidth() - 1;*/

                player.MoveRight(maze);
            }
            if (key.Key == ConsoleKey.B)
            {
                player.avatar--;
                player.PutInMaze(maze);
            }
            if (key.Key == ConsoleKey.N)
            {
                player.avatar++;
                player.PutInMaze(maze);
            }
            if (key.Key == ConsoleKey.Spacebar)
            {
                Console.WriteLine("Char value: " + (int)player.avatar);
                Console.ReadLine();
            }




        }
    }
}
