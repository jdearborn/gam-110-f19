﻿using System;

class Player
{
    int row;
    int col;
    public char avatar;
    char currentTile;

    public Player()
    {
        row = 3;
        col = 3;
        avatar = '\u263B';
        currentTile = ' ';
    }

    public void PutInMaze(Maze maze)
    {
        currentTile = maze.GetTile(row, col);
        maze.SetTile(row, col, avatar);
    }

    public void MoveTo(Maze maze, int newRow, int newCol)
    {
        if (maze.IsWalkable(newRow, newCol))
        {
            // Replacing where you're coming from
            maze.SetTile(row, col, currentTile);

            // Update position
            row = newRow;
            col = newCol;

            char newTile = maze.GetTile(row, col);
            if(newTile == 'A')
            {
                maze.SetTile(row, col, ' ');  // Cover up the 'A'
                maze.SetTile(2, 6, ' ');  // Cover up the $

                Console.Clear();
                Console.WriteLine("You stepped on A.  Press enter to continue...");
                Console.ReadLine();
            }

            // Change the maze to show your avatar there
            PutInMaze(maze);
        }
    }

    public void MoveLeft(Maze maze)
    {
        MoveTo(maze, row, col - 1);
    }

    public void MoveRight(Maze maze)
    {
        MoveTo(maze, row, col + 1);
    }

    public void MoveUp(Maze maze)
    {
        MoveTo(maze, row - 1, col);
    }

    public void MoveDown(Maze maze)
    {
        MoveTo(maze, row + 1, col);
    }

}