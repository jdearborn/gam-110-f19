﻿using System;
using System.Collections.Generic;


class Animal
{
    public virtual void Speak()
    {
        // What does a generic animal do???
    }
}

class Dog : Animal
{
    public int barkiness;

    public Dog()
    {
        barkiness = 3;
    }

    public override void Speak()
    {
        for(int i = 0; i < barkiness; ++i)
        {
            Console.WriteLine("Bark!");
        }
    }
}

class Cat : Animal
{
    public override void Speak()
    {
        Console.WriteLine("Meow");
    }

    public void Attack()
    {
        Console.WriteLine("The cat claws its enemy.");
    }
}

class Beagle : Dog
{
    public Beagle()
    {
        barkiness = 1;
    }

    public override void Speak()
    {
        base.Speak();

        Console.WriteLine("^ That was from a Beagle!");
    }
}

class Program
{
    static void Main(string[] args)
    {
        Dog sparky = new Dog();

        //sparky.Speak();


        Cat lou = new Cat();

        //lou.Speak();

        List<Animal> allMyPets = new List<Animal>();

        allMyPets.Add(sparky);
        allMyPets.Add(lou);
        allMyPets.Add(new Dog());
        allMyPets.Add(new Beagle());

        for (int i = 0; i < allMyPets.Count; ++i)
        {
            allMyPets[i].Speak();
        }


        Console.ReadLine();
    }
}