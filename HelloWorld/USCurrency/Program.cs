﻿using System;

class MyClass
{
    public int someNumber = 0;

    public void SomeFunction()
    {
        someNumber++;
    }
}

struct USCurrency
{
    private uint dollars;
    private uint cents;  // 0 to 99

    public USCurrency(uint nDollars, uint nCents)
    {
        dollars = nDollars;
        cents = nCents;

        while(cents > 99)
        {
            dollars++;
            cents -= 100;
        }
    }

    public void Add(USCurrency other)
    {
        dollars += other.dollars;
        cents += other.cents;

        if(cents > 99)
        {
            dollars++;
            cents -= 100;
        }
    }


    // Accessors for private members
    public uint GetDollars()
    {
        return dollars;
    }

    public uint GetCents()
    {
        return cents;
    }
}

class Program
{
    static void AddMoneysByValue(USCurrency amount)
    {
        amount.Add(new USCurrency(17, 0));
    }

    static void AddMoneysByReference(ref USCurrency amount)
    {
        amount.Add(new USCurrency(17, 0));
    }

    static void Get10Moneys(out USCurrency amount)
    {
        amount = new USCurrency(10, 0);
    }

    static void AddInt(int a)
    {
        a += 40;
    }

    static void Main(string[] args)
    {
        USCurrency amount1 = new USCurrency(1, 300);
        USCurrency amount2 = new USCurrency(4, 75);
        
        Console.WriteLine("Starting: " + amount1.GetDollars() + "." + amount1.GetCents());
        amount1.Add(amount2);

        Console.WriteLine("After Add(): " + amount1.GetDollars() + "." + amount1.GetCents());

        AddMoneysByValue(amount1);
        Console.WriteLine("After AddMoneysByValue(): " + amount1.GetDollars() + "." + amount1.GetCents());

        AddMoneysByReference(ref amount1);
        Console.WriteLine("After AddMoneysByReference(): " + amount1.GetDollars() + "." + amount1.GetCents());

        int b = 5;
        AddInt(b);

        USCurrency tenBucks;
        Get10Moneys(out tenBucks);


        Console.ReadLine();
    }
}
