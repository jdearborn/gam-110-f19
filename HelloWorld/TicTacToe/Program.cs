﻿using System;

class Program
{
    static void Main(string[] args)
    {
        // Make a 3x3 array
        char[,] board = {
            {'1', '2', '3'},
            {'4', '5', '6'},
            {'7', '8', '9'}
        };  // Process: row = (choice-1)/3,  column = (choice-1) % 3

        Console.WriteLine("Hey!  Let's play Tic Tac Toe!");
        Console.WriteLine("Press Enter to play");
        Console.ReadLine();

        int currentPlayer = 1;

        while(true)
        {
            Console.Clear();

            // Draw the board
            Console.WriteLine("---------------");
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    Console.Write("| " + board[i, j] + " |");
                }
                Console.WriteLine();
                Console.WriteLine("---------------");
            }

            do
            {
                Console.WriteLine("Player " + currentPlayer + ", please choose a spot: ");
                string input = Console.ReadLine();
                if (input == "quit" || input == "q")
                    return;

                // Interpret the choice
                int choice;
                if (int.TryParse(input, out choice) && choice >= 1 && choice <= 9)
                {
                    // Got a number, now what?
                    int row = (choice - 1) / 3;
                    int column = (choice - 1) % 3;

                    if (board[row, column] == 'X' || board[row, column] == 'O')
                    {
                        Console.WriteLine("Don't be a jerk, that spot is taken!");
                        continue;
                    }

                    // Place that marker
                    if (currentPlayer == 1)
                        board[row, column] = 'X';
                    else
                        board[row, column] = 'O';

                    bool youWin = false;
                    // Did you win horizontally?
                    for (int r = 0; r < 3; ++r)
                    {
                        if (board[r, 0] == board[r, 1] && board[r, 1] == board[r, 2])
                        {
                            youWin = true;
                        }
                    }
                    // Did you win vertically?
                    for (int c = 0; c < 3; ++c)
                    {
                        if (board[0, c] == board[1, c] && board[1, c] == board[2, c])
                        {
                            youWin = true;
                        }
                    }

                    // Did you win diagonally?
                    if((board[0,0] == board[1,1] && board[1,1] == board[2,2])
                        || (board[2, 0] == board[1, 1] && board[1, 1] == board[0, 2]))
                    {
                        youWin = true;
                    }

                    if(youWin)
                    {
                        Console.WriteLine("Player " + currentPlayer + " wins!");
                        Console.ReadLine();
                        return;
                    }

                    // Is it a draw?
                    bool isDraw = true;
                    for(int r = 0; r < 3; ++r)
                    {
                        for(int c = 0; c < 3; ++c)
                        {
                            if(board[r, c] != 'X' && board[r, c] != 'O')
                            {
                                isDraw = false;
                            }
                        }
                    }

                    if(isDraw)
                    {
                        Console.WriteLine("I expected better...  You both lose.");
                        Console.ReadLine();
                        return;
                    }


                    break;
                }
                else
                {
                    Console.WriteLine("You are stupid, give me a NUMBER (1-9)!");
                }
            } while (true);

            // Switch to the next player
            if (currentPlayer == 1)
                currentPlayer = 2;
            else
                currentPlayer = 1;
        }
    }
}
