﻿using System;


class Program
{
    static int Add(int a, int b)
    {
        return a + b;
    }

    static int Add5(int a)
    {
        return a + 5;
    }

    static void PrintSum(int first, int second, int third)
    {
        Console.WriteLine("Sum: " + (first + second + third));
    }

    static int Max(int a, int b, int c)
    {
        if (a > b && a > c)
            return a;
        if (b > a && b > c)
            return b;
        if (c > a && c > b)
            return c;

        return a;


        /*if (a > b)
        {
            if (a > c)
                return a;
            else
                return c;
        }
        else
        {
            if (b > c)
                return b;
            else
                return c;
        }*/
    }

    static int Max(int[] numbers)
    {
        // Find the greatest of all these numbers
        int biggest = 0;
        if (numbers.Length > 0)
            biggest = numbers[0];

        for(int i = 0; i < numbers.Length; ++i)
        {
            // Compare biggest to numbers[i]
            if (biggest < numbers[i])
                biggest = numbers[i];
        }
        return biggest;
    }

    static void Main(string[] args)
    {
        int num1 = 4;
        int num2 = 7;


        int result = Add(num1, num2);

        Console.WriteLine(num1 + " + " + num2 + " = " + result);


        Console.WriteLine("17 plus 5 is: " + Add5(17));
        Console.WriteLine("17 plus 5 plus 5 is: " + Add5(Add5(17)));

        PrintSum(10, 20, 40);

        Console.ReadLine();

    }
}