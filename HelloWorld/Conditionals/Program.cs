﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Please enter a number:");

        string input = Console.ReadLine();
        Console.WriteLine("You gave me " + input);

        //int number = int.Parse(input);
        int number;
        bool parseSuccess = int.TryParse(input, out number);
        if (parseSuccess)
        {
            if (number < 4)
            {
                Console.WriteLine("That is a low number!");
            }
            else
            {
                Console.WriteLine("That is a BIG number!");
            }
        }
        else
        {
            Console.WriteLine("You obviously don't know what a number is!");
        }


        Console.ReadLine();
    }
}