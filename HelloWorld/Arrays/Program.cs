﻿using System;


class Program
{
    static void Main(string[] args)
    {
        // 90, 93, 82, 68, 100, 54, 101, 84, 93, 103, 77, 76, 93, 85, 70
        int[] numbers = { 90, 93, 82, 68, 100, 54, 101, 84, 93, 103, 77, 76, 93, 85, 70 };
        /*numbers[0] = 90;
        numbers[1] = 93;*/

        int sum = 0;
        for(int i = 0; i < numbers.Length; ++i)
        {
            sum += numbers[i];
        }

        Console.WriteLine("Sum: " + sum);

        float average = sum / (float)numbers.Length;

        Console.WriteLine("Average: " + average);

        Console.ReadLine();
    }
}
