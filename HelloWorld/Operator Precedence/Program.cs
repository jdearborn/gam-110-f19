﻿using System;

class Program
{
    static void Main(string[] args)
    {
        // Integer division
        int a = 4;
        int b = 3;

        int c = a / b;  // 4/3 = 1
        int d = b / a;  // 3/4 = 0

        int remainder = a % b;  // 1


        // Division:  0, 0, 0, 1, 1, 1, 2, 2, 2
        // Remainder: 0, 1, 2, 0, 1, 2, 0, 1, 2

        // Floating point division
        float f = 3f / 4;


        // Yuck.
        int x = 2, y = 3;
        Console.WriteLine(3 * x / 2 * y - 1);

        Console.ReadLine();
    }
}