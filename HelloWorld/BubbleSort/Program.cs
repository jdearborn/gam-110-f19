﻿using System;
using System.Collections.Generic;

class Program
{
    static void BubbleSort(List<int> values)
    {
        // Repeat until no swaps are made
        while (true)
        {
            bool didASwapHappen = false;

            // Go through the array
            for (int i = 0; i < values.Count - 1; ++i)
            {
                // For each pair, swap them if they're out of order
                if (values[i] > values[i + 1])
                {
                    int temp = values[i];
                    values[i] = values[i + 1];
                    values[i + 1] = temp;

                    didASwapHappen = true;
                }
            }

            if (!didASwapHappen)
                break;
        }
    }

    static void Main(string[] args)
    {
        // Create new, empty list
        List<int> numbers = new List<int>();

        // Get random values to put in list
        Random rnd = new Random();
        for(int i = 0; i < 10; ++i)
        {
            numbers.Add(rnd.Next(0, 20));
        }

        // Print out the list
        for(int i = 0; i < numbers.Count; ++i)
        {
            Console.Write(numbers[i] + " ");
        }
        Console.WriteLine();

        // Sort the list
        BubbleSort(numbers);

        // Print the sorted list
        for (int i = 0; i < numbers.Count; ++i)
        {
            Console.Write(numbers[i] + " ");
        }
        Console.WriteLine();


        Console.ReadLine();

    }
}