﻿using System;


class Dog
{
    private string name;
    private float weight;
    public string breed;
    int barkiness;

    // Constructor (default)
    public Dog()
    {
        name = "Scruffy";
        weight = 100.0f;
        breed = "Cocker Spaniel";
        barkiness = 3;
    }

    public Dog(string newName, string newBreed, int newBarkiness)
    {
        name = newName;
        weight = 100.0f;
        breed = newBreed;
        barkiness = newBarkiness;
    }

    public void Bite()
    {
        Console.WriteLine("You have been bitten by " + name + "!");
    }

    public void Bark()
    {
        for(int i = 0; i < barkiness; ++i)
        {
            Console.WriteLine("Bark!");
        }
    }

    public string GetName()
    {
        return name;
    }

    public void DoThing(int a)
    {
        Console.WriteLine(a);
    }
    public void DoThing(long a)
    {
        Console.WriteLine(a);
    }

}