﻿using System;

class Program
{
    static void PrintAThing()
    {
        Console.WriteLine("Hello");
    }

    static void Main(string[] args)
    {
        PrintAThing();

        Dog myDog = new Dog();

        myDog.Bite();
        myDog.Bark();


        Dog myOtherDog = new Dog("Buddy", "Chihuahua", 10);

        myOtherDog.Bite();
        myOtherDog.Bark();

        Console.ReadLine();
    }
}