﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("What is the mass of Planet A?");
        string input = Console.ReadLine();

        double massA;
        if(!double.TryParse(input, out massA))
        {
            Console.WriteLine("That's not a number!");
            return;
        }

        double squareRoot = Math.Sqrt(massA);

        // Do more calculations!
    }
}