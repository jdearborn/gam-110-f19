﻿using System;
using System.Text;


class Program
{
    static string GetRandomWord(string[] words)
    {
        Random rnd = new Random();

        int n = rnd.Next(0, words.Length);

        return words[n];
    }

    // Returns a StringBuilder with underscores in place of each letter
    static StringBuilder CreatePuzzle(string answerWord)
    {
        StringBuilder result = new StringBuilder();

        // TODO: Handle spaces in the word specially?
        int numUnderscores = answerWord.Length;
        for(int i = 0; i < numUnderscores; ++i)
        {
            result.Append("_ ");
        }

        return result;
    }

    static void PrintMan(int incorrectGuesses)
    {
        /*
          
          /--
         |  |
         |  O
         | /|\
         |  |
         | / \
         |
         _______
         
         */
        if (incorrectGuesses == 0)
        {
            Console.WriteLine("       ");
            Console.WriteLine("       ");
            Console.WriteLine("       ");
            Console.WriteLine("       ");
            Console.WriteLine("       ");
            Console.WriteLine("       ");
            Console.WriteLine("       ");
            Console.WriteLine("       ");
        }
        else if(incorrectGuesses == 1)
        {
            Console.WriteLine("       ");
            Console.WriteLine("       ");
            Console.WriteLine("       ");
            Console.WriteLine("       ");
            Console.WriteLine("       ");
            Console.WriteLine("       ");
            Console.WriteLine("       ");
            Console.WriteLine("_______");
        }
        else
        {
            Console.WriteLine("/--    ");
            Console.WriteLine("|  |   ");
            Console.WriteLine("|  O   ");
            Console.WriteLine("| /|\\  ");
            Console.WriteLine("|  |   ");
            Console.WriteLine("| / \\  ");
            Console.WriteLine("|      ");
            Console.WriteLine("_______");
        }
    }

    static void PrintPuzzle(StringBuilder puzzle, StringBuilder guessedLetters)
    {
        Console.WriteLine("Letters in word: " + puzzle.Length/2);
        Console.WriteLine(puzzle + " / " + guessedLetters);
    }

    static bool GuessLetter(ref int incorrectCount, ref StringBuilder puzzle, 
        ref StringBuilder guessedLetters, string answer, char guess)
    {
        /*
         Appends the guessed letter to the guessedLetters StringBuilder. If the guess is
in the answer, replaces any underscores in the puzzle that match. If the guess is a miss,
increment the number of incorrect guesses
*/
        guessedLetters.Append(guess);

        bool correctGuess = false;
        for (int i = 0; i < answer.Length; ++i)
        {
            if(answer[i] == guess)
            {
                //  "_ "
                puzzle[i*2] = guess;
                correctGuess = true;
            }
        }

        if (!correctGuess)
            incorrectCount++;

        return correctGuess;
    }

    static void Main(string[] args)
    {
        string[] words = {
            "video game",
            "bean",
            "kurdistan", 
            "uruguay",
            "keyboard",
            "boolean",
            "program",
            "stupid"
        };

        string answerWord = GetRandomWord(words);
        Console.WriteLine("THE SECRET ANSWER: " + answerWord);

        StringBuilder puzzle = CreatePuzzle(answerWord);

        Console.WriteLine("THE PUZZLE: " + puzzle);

        StringBuilder guessedLetters = new StringBuilder();
        int incorrectCount = 0;

        Console.WriteLine("Press Enter to begin.");
        Console.ReadLine();

        while (incorrectCount < 7)
        {

            PrintMan(incorrectCount);

            PrintPuzzle(puzzle, guessedLetters);

            Console.WriteLine("Please pick a letter.");
            string input = Console.ReadLine();

            Console.Clear();
            if (input.Length == 1)
            {
                if (GuessLetter(ref incorrectCount, ref puzzle, ref guessedLetters, answerWord, input[0]))
                {
                    Console.WriteLine("You got it!");
                }
                else
                {
                    Console.WriteLine("That is incorrect.");
                }
            }

            /*if (CheckWin(incorrectCount, answer, puzzle))
            {
                Console.WriteLine("You win!");
                break;
            }
            if (CheckLose(incorrectCount))
            {
                Console.WriteLine("You are a stinky loser!");
                break;
            }*/
        }


        PrintMan(incorrectCount);

        Console.WriteLine("Game over!");


        Console.ReadLine();
    }
}
