﻿using System;

namespace Loops
{
    class Program
    {
        static void Main(string[] args)
        {
            // Count up from 0 to 19
            int i = 0;
            while(i < 20)
            {
                Console.WriteLine(i);
                ++i;
            }

            // Count down from 20 to 0
            i = 20;
            while(i >= 0)
            {
                Console.WriteLine(i);
                --i;
            }

            for(int j = 0; j < 20; ++j)
            {
                Console.WriteLine(j);
            }

            for(int j = 0; j < 50; ++j)
            {
                if(j == 42)
                {
                    // continue;
                    break;
                }
                Console.WriteLine(j);
            }

            Console.ReadLine();
        }
    }
}
