﻿using System;

namespace Practice7
{
    class Program
    {
        static void Main(string[] args)
        {
            for(int i = 1; i <= 10; ++i)
            {
                Console.WriteLine(i);
            }

            for(int i = 3; i <= 99; i += 3)
            {
                Console.WriteLine(i);
            }
            
            string widthString = Console.ReadLine();
            string heightString = Console.ReadLine();

            int width;
            int height;
            if(int.TryParse(widthString, out width) && int.TryParse(heightString, out height))
            {
                for (int h = 0; h < height; ++h)
                {
                    for (int w = 0; w < width; ++w)
                    {
                        Console.Write("*");
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
