﻿using System;

class Program
{
    static void Main(string[] args)
    {
        int[,] array = { {0, 1, 2},
                         {3, 4, 5},
                         {6, 7, 8}
                       };


        Console.WriteLine("-------------");
        for (int row = 0; row < 3; ++row)
        {
            Console.WriteLine("| " + array[row, 0] + " | " + array[row, 1] + " | " + array[row, 2] + " |");
            Console.WriteLine("-------------");
        }

        Console.ReadLine();
    }
}