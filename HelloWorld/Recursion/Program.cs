﻿using System;

class Program
{
    static void Countdown(int start)
    {
        // Iteratively:
        /*for(int i = start; i >= 0; --i)
        {
            Console.WriteLine(i);
        }*/

        // Recursively:
        if (start < 0)
            return;

        Console.WriteLine(start);
        Countdown(start-1);
    }

    static int Factorial(int n)
    {
        if (n == 0)
            return 1;

        return n * Factorial(n - 1);
    }

    static int Fibonacci(int n)
    {
        if (n == 0)
            return 0;
        if (n == 1)
            return 1;

        return Fibonacci(n - 1) + Fibonacci(n - 2);
    }

    static void Main(string[] args)
    {
        Countdown(10);

        Console.WriteLine("Factorial(4): " + Factorial(4));


        Console.WriteLine("Fibonacci(12): " + Fibonacci(12));

        Console.ReadLine();
    }
}